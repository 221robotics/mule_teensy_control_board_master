float joyMapWithDead(float joystickData, float joystickLowPos, float joystickCenterPos, float joystickHighPos, float MinOut, float MaxOut, float deadBand)
{
  if(joystickData > joystickCenterPos + deadBand)
  {
    joystickData = mapFloat(joystickData, joystickCenterPos+deadBand, joystickHighPos, 0.0f, MaxOut);
    joystickData = constrain(joystickData, 0.0f, MaxOut);
    return joystickData;
  }
  else if(joystickData < joystickCenterPos-deadBand)
  {
    joystickData = mapFloat(joystickData, joystickLowPos, joystickCenterPos-deadBand, MinOut, 0.0f);
    joystickData = constrain(joystickData, MinOut, 0.0f);
    return joystickData;
  }
  else
  {
     joystickData = 0.0f;
     return joystickData;
  }
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
 return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

//***********************************************************************************************************
//Adds a cubic sensitivity adjustment to the input value.  deadBand is range (0-1).  cubicGain is range (0-1)
//***********************************************************************************************************
static float joyCubic(float input, float cubicGain)
{
  float i = cubicGain * (input * input * input);
  float j = (1.0f - cubicGain) * input;
  return i + j;            
}

int checkTurns(int input, int jumpNum)
{
  static int inputLast = 0;
  static int turns = 0;
    
  if((input - inputLast) <= -jumpNum)
    turns += 1;
  
  if((input - inputLast) >= jumpNum)
    turns -= 1;
  
  inputLast = input;
  return turns;
}

//bring in a single turn absolute sensor and wrap it at the zero-crossing
float wrapAngle(float input, int turns, int sensorRange)
{
  return float(input + (sensorRange * turns));  //360 total degrees of sensor reading
}

float applyLimits(float value, float lowerLimit, float upperLimit) {
  if (value < lowerLimit) {
    return lowerLimit;
  }
  else if (value > upperLimit) {
    return upperLimit;
  }
  else {
    return value;
  }
}

float applyRamp(float value, float previousValue, float negativeDelta, float positiveDelta)
{
  float commandDelta = value - previousValue;

  // flip our limits when the value is negative (so that we scale appropriately in both directions)
  if (previousValue >= 0) {
    commandDelta = applyLimits(commandDelta, negativeDelta, positiveDelta);
  } else {
    commandDelta = applyLimits(commandDelta, -positiveDelta, -negativeDelta);
  }

  previousValue += commandDelta;

  return previousValue;
}

void longToBytes(unsigned long val, byte *buf)
{
  union u_tag {
   byte b[4];
   unsigned long lval;
  } u;

  u.lval = val;

  buf[0] = u.b[3];
  buf[1] = u.b[2];
  buf[2] = u.b[1];
  buf[3] = u.b[0];
}

static void hexDump(uint8_t dumpLen, uint8_t *bytePtr)
{
  uint8_t working;
  while( dumpLen-- ) {
    working = *bytePtr++;
    Serial.write( hex[ working>>4 ] );
    Serial.write( hex[ working&15 ] );
  }
  Serial.write('\r');
  Serial.write('\n');
}

void scanI2CWire1(void)
{
  Wire1.begin();
  
  Serial.println ();
  Serial.println ("I2C scanner. Scanning Wire...");
  byte count = 0;

  for (byte i = 1; i < 120; i++)
  {
    Wire1.beginTransmission (i);
    if (Wire1.endTransmission () == 0)
      {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
      } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
}
