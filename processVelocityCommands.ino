void processVelocityCommands()
{
  //read incoming velocity requests and constrain to max vehicle speed
  //leftDriveFrequencyRequest = constrain(xAxisFromRemote - -yAxisFromRemote, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY);
  //rightDriveFrequencyRequest = constrain(xAxisFromRemote + -yAxisFromRemote, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY);
/*
  if(leftDriveFrequencyRequest >= 0 )
  {
    leftDriveFrequencyRequest = map(leftDriveFrequencyRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 3000, 1000);
    motor1DACOutput.setVoltage(0, false);
  }
  else
  {
    leftDriveFrequencyRequest = map(leftDriveFrequencyRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 1000, 3000);
    motor1DACOutput.setVoltage(4095, false);
  }

  if(rightDriveFrequencyRequest >= 0 )
  {
   rightDriveFrequencyRequest = map(rightDriveFrequencyRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 3000, 1000);
   motor2DACOutput.setVoltage(4095, false);
  }
  else
  {
    rightDriveFrequencyRequest = map(rightDriveFrequencyRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 1000, 3000);
    motor2DACOutput.setVoltage(0, false);
  }*/
  
  leftWheelVelocityRequest = constrain(leftWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY);
  rightWheelVelocityRequest = constrain(rightWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY);

  leftWheelVelocityRequest = 10;
  //rightWheelVelocityRequest = 45;
  
  leftDriveFrequencyRequest = map(leftWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 3000, 1000);   
  rightDriveFrequencyRequest = map(rightWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 3000, 1000);
       
  if(leftWheelVelocityRequest <= 0 )
  {
    leftDriveFrequencyRequest = map(leftWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 1000, 3000);
    motor2DACOutput.setVoltage(0, false);
  }
  else
  {
    motor2DACOutput.setVoltage(4095, false);
  }
  
  if(rightWheelVelocityRequest <= 0 )
  {
    rightDriveFrequencyRequest = map(rightWheelVelocityRequest, -MAX_MULE_VELOCITY, MAX_MULE_VELOCITY, 1000, 3000);
    motor1DACOutput.setVoltage(4095, false);
  }
  else
  {
    motor1DACOutput.setVoltage(0, false);
  }

  //if(leftWheelVelocityRequest <= 0 && rightWheelVelocityRequest >= 0)
  //{
    //motor1DACOutput.setVoltage(4095, false);
    //motor2DACOutput.setVoltage(4095, false);
  //}
}

