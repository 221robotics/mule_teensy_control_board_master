#define PACKET_TIMEOUT_MILLISECONDS   1000
#define ENABLE_TIMEOUT_MILLISCONDS    110
#define NUM_LEDS                      (144*3) - 57
#define DATA_PIN                      A4
#define CLOCK_PIN                     A5
#define KILL_SWITCH_LOOP_PIN          A3
#define INDICATOR_LED                 13

#define MOTOR_1_ENABLE                17 
#define MOTOR_2_ENABLE                14 
#define MOTOR_1_INPUT_B               16
#define MOTOR_2_INPUT_B               15

#define MOTOR_1_ENCODER_A             35
#define MOTOR_1_ENCODER_B             36
#define MOTOR_2_ENCODER_A             39
#define MOTOR_2_ENCODER_B             22

#define ENCODER_PPR                   1024
#define VELOCITY_FILTER_GAIN          0.9f

#define MAX_MOTOR_RPM                 1000.0f
#define DRIVE_GEAR_RATIO              300.0f/14.0f
#define MAX_MULE_VELOCITY             46.6666666667 //MAX_MOTOR_RPM/DRIVE_GEAR_RATIO 

#define ENCODER_QUAD_COUNTS_PER_REVOLUTION    ENCODER_PPR * 4.0f
#define TICKS_PER_RADIAN                      1.0f/(ENCODER_QUAD_COUNTS_PER_REVOLUTION/TWO_PI);
#define RADIANS_PER_TICK                      TWO_PI/ENCODER_QUAD_COUNTS_PER_REVOLUTION;
#define TICKS_PER_REVOLUTION                  (1.0f/ENCODER_QUAD_COUNTS_PER_REVOLUTION);
#define RADIANS_TO_RPM                        9.55f

#define ENCODER_OPTIMIZE_INTERRUPTS

double dT = 0.0;

//#define leftDrive                     5
//#define rightDrive                    3

//DotStar LED init
CRGB leds[NUM_LEDS];

Encoder motor1Encoder(MOTOR_1_ENCODER_A, MOTOR_1_ENCODER_B);
Encoder motor2Encoder(MOTOR_2_ENCODER_A, MOTOR_2_ENCODER_B);

long motor1EncoderAccumulation = -999;
long motor2EncoderAccumulation = -999;
float motor1Velocity = 0.0f;
float motor2Velocity = 0.0f;
float motor1VelocityUnfiltered = 0.0f;
float motor2VelocityUnfiltered = 0.0f;
float motor1VelocityUnfilteredRPM = 0.0f;
float motor2VelocityUnfilteredRPM = 0.0f;
float motor1Radians = 0.0f;
float motor2Radians = 0.0f;
float motor1RadiansPrevious = 0.0f;
float motor2RadiansPrevious = 0.0f;
float motor1DeltaRadians = 0.0f;
float motor2DeltaRadians = 0.0f;
float wheel1VelocityRPM = 0.0f;
float wheel2VelocityRPM = 0.0f;

//digital to analog chip init
Adafruit_MCP4725 motor1DACOutput;
Adafruit_MCP4725 motor2DACOutput;
#define DAC_RESOLUTION    (8)

void RampToVelocity(int finalVelocity, double RampTime=0);
void DoSinusoid(int Amplitude,double Period);

int CurrentVelocity=0;
int VelocityResolution=1;

int DwellTime = 500; // desired time (ms) for ramp and constant velocity
int powerLEDToggle = 0;
int blinkToggle = 0;
int sinWave;
int leftDriveSpeedRaw;
int pulse = 0;
int squareWaveLeft = 255;
int squareWaveRight = 0;
int xAxisFromRemote, yAxisFromRemote, leftDriveFrequencyRequest, rightDriveFrequencyRequest;
int leftWheelVelocityRequest, rightWheelVelocityRequest;
int deadManSwitch, killSwitch, switch1FromRemote, switch2FromRemote, emptyFromRemote;
boolean radioDataReceived = false;
uint8_t gHue = 0;

unsigned long lastPacket = 0;
unsigned long lastPacketFreqCounter = 0;

float loopTimeHz = 0.0f;
float loopTimeHzRight = 0.0f;
int frequencyToggle = 0;
static uint8_t hex[17] = "0123456789abcdef";

//Serial Input Variables//
boolean readyToParse = false;

String deadManSwitchStr;
String xAxisJoystickStr;
String yAxisJoystickStr;
String joystickSwitch1Str;
String joystickSwitch2Str;
String emptyByteStr;
String* currentVarEE;

enum imutype 
{
  deadManSwitchSerial,
  xAxisJoystickSerial,
  yAxisJoystickSerial
};

imutype currentVar;

int ledState;

unsigned long loopLeftDriveFrequencyGlobal, loopRightDriveFrequencyGlobal;
