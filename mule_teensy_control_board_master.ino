#include <FastLED.h>
#include "math.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <FlexCAN.h>
#include <Encoder.h>
#include <Adafruit_MCP4725.h>
#include "Globals.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

void setup() 
{
  //scanI2CWire1();
  
  FastLED.addLeds<DOTSTAR, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);
  
  //Define I/O pins  
  pinMode(INDICATOR_LED, OUTPUT);
  pinMode(MOTOR_1_ENABLE, OUTPUT);
  pinMode(MOTOR_2_ENABLE, OUTPUT);  
  pinMode(MOTOR_1_INPUT_B, OUTPUT);
  pinMode(MOTOR_2_INPUT_B, OUTPUT);
  pinMode(KILL_SWITCH_LOOP_PIN, INPUT_PULLUP);  

  //start I2C digital to analog converter and define address
  motor1DACOutput.begin(0x62);
  motor2DACOutput.begin(0x63);

  //write neutral drive powers out to the DAC modules
  motor1DACOutput.setVoltage(2048, false);
  motor2DACOutput.setVoltage(2048, false);

  //Start Serial output for dubugging
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial3.begin(9600);
  
  Serial.println("eStop Receiver Init");
  
  // start off by ensuring thet the motor is disabled before proceeding
  digitalWrite(MOTOR_1_ENABLE, LOW);
  digitalWrite(MOTOR_2_ENABLE, LOW);
  Serial.println("enable set low");
  delay(DwellTime);
}
// end of setup code

void tenMs()
{
  
}

void fiftyMs()
{
  //blink main power LED at 50ms
  powerLEDToggle = !powerLEDToggle;
  digitalWrite(13, powerLEDToggle);     

  //used to control the flashing status LEDs
  blinkToggle = !blinkToggle;

  processEncoders();
  processVelocityCommands();
  
  //FastLED.show();
  
  //Print debug messages to console
  Serial.print('H'); 
  Serial.print(",");
  Serial.print(deadManSwitch);
  Serial.print(",");
  Serial.print(switch1FromRemote);
  Serial.print(",");
  Serial.print(switch2FromRemote);
  Serial.print(",");
  Serial.print(leftWheelVelocityRequest);
  Serial.print(",");
  Serial.print(rightWheelVelocityRequest);
  Serial.print(",");
  Serial.print(leftDriveFrequencyRequest);
  Serial.print(",");
  Serial.print(rightDriveFrequencyRequest);
  Serial.print(",");
  Serial.print(loopTimeHz);//loopLeftDriveFrequencyGlobal);//wheel1VelocityRPM);
  Serial.print(",");
  Serial.print(loopTimeHzRight);//loopRightDriveFrequencyGlobal);//wheel2VelocityRPM);
  Serial.print(",");
  Serial.println('F'); 

  //Serial3.print(ledState);
  
}

void leftDriveFrequency()
{
  //frequencyToggle = !frequencyToggle;
  digitalWriteFast(MOTOR_1_INPUT_B, HIGH);
}

void rightDriveFrequency()
{
  //frequencyToggle = !frequencyToggle;
  digitalWriteFast(MOTOR_2_INPUT_B, HIGH);
}

// put your main code here, to loops indefinitely:
void loop()
{
  unsigned long lastLoopTimeTen = micros();
  unsigned long lastLoopTimeFifty = micros();
  unsigned long lastloopLeftDriveFrequency = micros();
  unsigned long lastloopRightDriveFrequency = micros();
  unsigned long lastLogicalTimeInMicroSeconds = micros();

  while (true)
  {
    unsigned long loopDiffTen = micros() - lastLoopTimeTen;
    unsigned long loopDiffFifty = micros() - lastLoopTimeFifty;
    
    unsigned long loopLeftDriveFrequency = micros() - lastloopLeftDriveFrequency;
    unsigned long loopRightDriveFrequency = micros() - lastloopRightDriveFrequency;

    readSerial1In();
    readSerialUSBIn();

    //Input B acts as a second kill signal...keep LOW to stay ENABLED
    digitalWrite(MOTOR_1_INPUT_B, LOW);  //Left Drive
    digitalWrite(MOTOR_2_INPUT_B, LOW); //Right Drive

    digitalWriteFast(MOTOR_1_ENABLE, deadManSwitch > 0 ? HIGH : LOW);
    digitalWriteFast(MOTOR_2_ENABLE, deadManSwitch > 0 ? HIGH : LOW);

    //digitalWrite(MOTOR_1_ENABLE, LOW); //does nothing...both motors are wired to MOTOR_2_ENABLE
    //digitalWrite(MOTOR_2_ENABLE, HIGH);

    /*
    if(deadManSwitch == HIGH && radioDataReceived == true)
    {
      int brightnessValue = map(blinkToggle, 0, 1, 0, 255);
      //fill_solid(leds, NUM_LEDS, CRGB::Green);
      //LEDS.setBrightness(brightnessValue);
      ledState = 3;
    }
    else if(radioDataReceived == false)
    {
      int brightnessValue = map(blinkToggle, 0, 1, 0, 255);
      //fill_solid(leds, NUM_LEDS, CRGB::Blue);
      //LEDS.setBrightness(brightnessValue);
      ledState = 2;
    }
    else
    {
      int brightnessValue = map(blinkToggle, 0, 1, 0, 255);
      fill_solid(leds, NUM_LEDS, CRGB::Red);
      LEDS.setBrightness(brightnessValue);
      ledState = 1;
    }*/
    
    if ((millis() - lastPacket) >= PACKET_TIMEOUT_MILLISECONDS)
    {
      radioDataReceived = false;
      digitalWrite(MOTOR_1_ENABLE, LOW);
      digitalWrite(MOTOR_2_ENABLE, LOW);
    }

    if (loopDiffTen >= 10000) 
    {
      tenMs();
      //loopTimeHz = 1000000.0f / loopDiffTen;
      lastLoopTimeTen = micros();
    }   

    if (loopDiffFifty >= 50000) //50ms
    {
      fiftyMs();
      dT = (float)loopDiffFifty/1000000.0f;
      lastLoopTimeFifty = micros();
    }

    if (loopLeftDriveFrequency >= leftDriveFrequencyRequest) //1Hz
    {
      loopLeftDriveFrequencyGlobal =loopLeftDriveFrequency;
      leftDriveFrequency();      
      loopTimeHz = 1000000.0f / loopLeftDriveFrequency;
      lastloopLeftDriveFrequency = micros();
    }

    if (loopRightDriveFrequency >= rightDriveFrequencyRequest) 
    {
      loopRightDriveFrequencyGlobal = loopRightDriveFrequency;
      rightDriveFrequency();    
      loopTimeHzRight = 1000000.0f / loopRightDriveFrequency;  
      lastloopRightDriveFrequency = micros();
    }
  }
}
