void readSerial1In()
{
  while (Serial1.available() > 0) 
  {    
    char c = Serial1.read();
    //Serial.print(c);
    
      if (c == 'H') 
      {      
        //Serial1.read();
        
        radioDataReceived = true;
        lastPacket = millis();

        deadManSwitchStr = String("");
        joystickSwitch1Str = String("");
        joystickSwitch2Str = String("");
        xAxisJoystickStr = String("");
        yAxisJoystickStr = String("");
        readyToParse = true;
        currentVarEE = &emptyByteStr;
      } 
      else if (readyToParse) 
      {
        if (c == ',' || c == '\r') 
        {
          if (currentVarEE == &emptyByteStr) 
          {
            emptyFromRemote = emptyByteStr.toInt();
            currentVarEE = &deadManSwitchStr;
          } 
          else if (currentVarEE == &deadManSwitchStr) 
          {
            deadManSwitch = deadManSwitchStr.toInt();
            //Serial.println(deadManSwitch);
            currentVarEE = &joystickSwitch1Str;
          } 
          else if (currentVarEE == &joystickSwitch1Str) 
          {
            switch1FromRemote = joystickSwitch1Str.toInt();
            currentVarEE = &joystickSwitch2Str;
          } 
          else if (currentVarEE == &joystickSwitch2Str) 
          {
            switch2FromRemote = joystickSwitch2Str.toInt();
            currentVarEE = &xAxisJoystickStr;
          } 
          else if (currentVarEE == &xAxisJoystickStr) 
          {
            leftWheelVelocityRequest = xAxisJoystickStr.toInt();
            currentVarEE = &yAxisJoystickStr;
          } 
          else if (currentVarEE == &yAxisJoystickStr) 
          {
            rightWheelVelocityRequest = yAxisJoystickStr.toInt();
            readyToParse = false;
          } 
        } 
        else 
        {
          currentVarEE->concat(c);
        }
      }
    }
}


