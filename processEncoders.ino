void processEncoders()
{
  motor1EncoderAccumulation = motor1Encoder.read();
  motor2EncoderAccumulation = motor2Encoder.read();
    
  motor1Radians = motor1EncoderAccumulation * TICKS_PER_RADIAN;
  motor1DeltaRadians = motor1Radians - motor1RadiansPrevious;
  motor1VelocityUnfiltered = motor1DeltaRadians / dT; //Rad/s
  motor1VelocityUnfilteredRPM = motor1VelocityUnfiltered * RADIANS_TO_RPM;
  wheel1VelocityRPM = motor1VelocityUnfilteredRPM / (300.0f/14.0f);//DRIVE_GEAR_RATIO;
  
  motor2Radians = (float)motor2EncoderAccumulation * TICKS_PER_RADIAN;
  motor2DeltaRadians = motor2Radians - motor2RadiansPrevious;
  motor2VelocityUnfiltered = motor2DeltaRadians / dT; //Rad/s
  motor2VelocityUnfilteredRPM = motor2VelocityUnfiltered * RADIANS_TO_RPM;
  wheel2VelocityRPM = motor2VelocityUnfilteredRPM / (300.0f/14.0f);//DRIVE_GEAR_RATIO;
  
  if(motor1Radians != motor1RadiansPrevious)
  {
    motor1RadiansPrevious = motor1Radians;
  }

  if(motor2Radians != motor2RadiansPrevious)
  {
    motor2RadiansPrevious = motor2Radians;
  }
}

